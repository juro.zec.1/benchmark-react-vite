import React, { StrictMode, useCallback, useLayoutEffect, useState } from 'react'
import { render } from 'react-dom'
const WARMUP_ROUNDS = 4;

const A1 = ["Mandatory", "Open-architected", "Re-engineered", "Realigned", "Optimized", "Organized", "Advanced",
  "Multi-tiered", "Switchable", "Focused", "Self-enabling", "Stand-alone", "Virtual", "Enhanced", "Enterprise-wide",
  "Optional", "Versatile", "Profit-focused", "Adaptive", "Object-based", "Diverse", "Expanded", "Front-line",
  "Visionary", "Upgradable", "Exclusive"]; // length = 27

const A2 = ["bi-directional", "grid-enabled", "system-worthy", "uniform", "5th generation", "coherent", "optimal",
  "zero administration", "systematic", "dedicated", "static", "eco-centric", "dynamic", "homogeneous", "high-level",
  "multi-state", "foreground", "web-enabled", "radical", "next generation", "encompassing", "stable", "real-time",
  "didactic", "bifurcated", "scalable", "needs-based"]; // length = 26

const A3 = ["strategy", "firmware", "encryption", "throughput", "projection", "Graphical User Interface", "array",
  "adapter", "framework", "open system", "functionalities", "methodology", "instruction set", "collaboration",
  "infrastructure", "orchestration", "core", "intranet", "parallelism", "hierarchy", "challenge", "extranet",
  "software", "budgetary management", "flexibility"]; // length = 23

const grabFrom = (arr) => arr[Math.floor(Math.random() * arr.length)];

let currId = 1;

const createData = (count) => {
  const data = new Array(count);

  for (let i = 0; i < count; i++) {
    data[i] = {
      id: currId++,
      phrase: `${grabFrom(A1)} ${grabFrom(A2)} ${grabFrom(A3)}`,
    };
  }

  return data;
};

const Main = () => {
  const [data, setData] = useState([]);
  const [done, setDone] = useState(false);

  useLayoutEffect(() => {
    if (!performance.getEntriesByName("benchmark_start").length) return;
    performance.mark("benchmark_end");
    const measure = performance.measure("benchmark", "benchmark_start", "benchmark_end");
    console.log(measure);
    setDone(true);
  }, [data]);

  const initialize = useCallback(() => {
    const data = createData(1000);
    setData(data);
  }, [setData]);

  const create1K = useCallback(() => {
    // warmup rounds
    for (let w = 0; w < WARMUP_ROUNDS + 1; w++) {
      const data = createData(1000);
      if (w === WARMUP_ROUNDS) performance.mark("benchmark_start");
      setData(data);
    }
  }, [setData]);

  const create2K = useCallback(() => {
    // warmup rounds
    for (let w = 0; w < WARMUP_ROUNDS + 1; w++) {
      const data = createData(2000);
      if (w === WARMUP_ROUNDS) performance.mark("benchmark_start");
      setData(data);
    }
  }, [setData]);

  const create5K = useCallback(() => {
    // warmup rounds
    for (let w = 0; w < WARMUP_ROUNDS + 1; w++) {
      const data = createData(5000);
      if (w === WARMUP_ROUNDS) performance.mark("benchmark_start");
      setData(data);
    }
  }, [setData]);

  const create10K = useCallback(() => {
    const data = createData(10000);
    performance.mark("benchmark_start");
    setData(data);
  }, [setData]);

  const create25K = useCallback(() => {
    // warmup rounds
    const data = createData(25000);
    performance.mark("benchmark_start");
    setData(data);
  }, [setData]);

  const updateEvery10th = useCallback(() => {
    if (data.length < 1000) throw new Error('Not initialized');
    for (let w = 0; w < WARMUP_ROUNDS + 1; w++) {
      const newData = data.slice(0);
      for (let i = 0; i < newData.length; i += 10) {
        const {id, phrase} = newData[i];
        newData[i] = {id, phrase: phrase + ` (u${w + 1})`};
      }
      if (w === WARMUP_ROUNDS) performance.mark("benchmark_start");
      setData(newData);
    }
  }, [data, setData]);

  const swapTwo = useCallback(() => {
    if (data.length < 1000) throw new Error('Not initialized');
    for (let w = 0; w < WARMUP_ROUNDS + 1; w++) {
      const newData = [data[0], data[998], ...data.slice(2, 998), data[1], data[999]];
      if (w === WARMUP_ROUNDS) performance.mark("benchmark_start");
      setData(newData);
    }
  }, [data, setData]);

  const deleteOne = useCallback(() => {
    if (data.length < 1000) throw new Error('Not initialized');
    for (let w = 0; w < WARMUP_ROUNDS + 1; w++) {
      const newData = [...data.slice(0, 499), ...data.slice(500)];
      if (w === WARMUP_ROUNDS) performance.mark("benchmark_start");
      setData(newData);
    }
  }, [data, setData])

  const deleteEvery3rd = useCallback(() => {
    if (data.length < 1000) throw new Error('Not initialized');
    const newData = [...data].filter((r, i) => i % 3 !== 0);
    performance.mark("benchmark_start");
    setData(newData);
  }, [data, setData]);

  const deleteAll = useCallback(() => {
    if (data.length < 1000) throw new Error('Not initialized');
    performance.mark("benchmark_start");
    setData([]);
  }, [data, setData]);

  return (
    <>
      <button id="initialize" onClick={initialize}>Initialize</button>
      <button id="create1K" onClick={create1K}>Create 1.000</button>
      <button id="create2K" onClick={create2K}>Create 2.000</button>
      <button id="create5K" onClick={create5K}>Create 5.000</button>
      <button id="create10K" onClick={create10K}>Create 10.000</button>
      <button id="create25K" onClick={create25K}>Create 25.000</button>
      <button id="updateEvery10th" onClick={updateEvery10th}>Update every 10th</button>
      <button id="swapTwo" onClick={swapTwo}>Swap two</button>
      <button id="deleteOne" onClick={deleteOne}>Delete one</button>
      <button id="deleteEvery3rd" onClick={deleteEvery3rd}>Delete every 3rd</button>
      <button id="deleteAll" onClick={deleteAll}>Delete all</button>
      {done && <span id="isDone">Done</span>}
      <table>
        <thead>
        <tr>
          <th>ID</th>
          <th>Phrase</th>
        </tr>
        </thead>
        <tbody>
        {data.map(({id, phrase}) => (
          <tr key={id}>
            <td>{id}</td>
            <td>{phrase}</td>
          </tr>
        ))}
        </tbody>
      </table>
    </>
  )
}

render(<StrictMode><Main /></StrictMode>, document.getElementById('main'))
